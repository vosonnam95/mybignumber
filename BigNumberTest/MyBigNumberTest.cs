using BigNumCore.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BigNumberTest
{
    [TestClass]
    public class MyBigNumberTest
    {
        private IBigNum _bignum;

        public MyBigNumberTest()
        {
            _bignum = new MyBigNumber();
        }

        [TestMethod]
        public void LongString_Sum_ShortString_Successed()
        {
            Assert.AreEqual("3924449013655737314063291243513177641990121889724345005820823514910852020363586056599996509371323016", 
                _bignum.Sum("3924449013655737314063291243513177641990121889724345005820823514910852020363586049897426641737883753"
                , "6702569867633439263"));
        }

        [TestMethod]
        public void LongString_SumChar_ShortString_Successed()
        {
            Assert.AreEqual("3924449013655737314063291243513177641990121889724345005820823514910852020363586056599996509371323016",
                _bignum.SumChar("3924449013655737314063291243513177641990121889724345005820823514910852020363586049897426641737883753"
                , "6702569867633439263"));
        }

        [TestMethod]
        public void HasCarry_SumChar_Successed()
        {
            Assert.AreEqual("193486395699551229338398752347024151180419792", 
                _bignum.SumChar("96743197849775614669199376173512075590209896"
                , "96743197849775614669199376173512075590209896"));
        }

        [TestMethod]
        public void HasCarry_Sum_Successed()
        {
            Assert.AreEqual("193486395699551229338398752347024151180419792",
                _bignum.Sum("96743197849775614669199376173512075590209896"
                , "96743197849775614669199376173512075590209896"));
        }

        [TestMethod]
        public void Five_Sum_Five_Successed()
        {
            Assert.AreEqual("1111111111111111111110", 
                _bignum.Sum("555555555555555555555"
                , "555555555555555555555"));
        }

        [TestMethod]
        public void Five_SumChar_Five_Successed()
        {
            Assert.AreEqual("1111111111111111111110",
                _bignum.SumChar("555555555555555555555"
                , "555555555555555555555"));
        }

        [TestMethod]
        public void Sum_Zero_Successed()
        {
            Assert.AreEqual("3642332261314494131370550987677871136148605500780045613637257782920884209123719670438233814", 
                _bignum.Sum("3642332261314494131370550987677871136148605500780045613637257782920884209123719670438233814"
                , "0"));
        }

        [TestMethod]
        public void SumChar_Zero_Successed()
        {
            Assert.AreEqual("3642332261314494131370550987677871136148605500780045613637257782920884209123719670438233814",
                _bignum.SumChar("3642332261314494131370550987677871136148605500780045613637257782920884209123719670438233814"
                , "0"));
        }

        [TestMethod]
        public void Zero_Sum_Zero_Successed()
        {
            Assert.AreEqual("0",
                _bignum.Sum("0"
                , "0"));
        }

        [TestMethod]
        public void Zero_SumChar_Zero_Successed()
        {
            Assert.AreEqual("0",
                _bignum.SumChar("0"
                , "0"));
        }

        [TestMethod]
        public void Five_Sum_Five_Failed()
        {
            Assert.AreNotEqual("11111111111111111111111111111111110", 
                _bignum.Sum("5555555555555555555555555555555555"
                , "5555555555555555555555555555555555"));
        }

        [TestMethod]
        public void NegativeInteger_AddNum_PositiveInteger_Successed()
        {
            Assert.AreEqual("33862420416203371660518823415353609714233790",
                _bignum.AddNum(new BigInteger("-57280777433572243008680552758158465875976106")
                , new BigInteger("91143197849775614669199376173512075590209896")).ToString());
        }

        [TestMethod]
        public void PositiveInteger_AddNum_NegativeInteger_Successed()
        {
            Assert.AreEqual("-33862420416203371660518823415353609714233790",
                _bignum.AddNum(new BigInteger("57280777433572243008680552758158465875976106")
                , new BigInteger("-91143197849775614669199376173512075590209896")).ToString());
        }

        [TestMethod]
        public void AddNum_Zero_Successed()
        {
            Assert.AreEqual("0",
                _bignum.AddNum(new BigInteger("70686261683012186020658892604200262153114537135778286494952352972315")
                , new BigInteger("-70686261683012186020658892604200262153114537135778286494952352972315")).ToString());
        }
    }
}
