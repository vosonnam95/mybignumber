﻿using BigNumCore.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigNumberTest
{
    [TestClass]
    public class NewBigNumberTest
    {
        private IBigNum _bignum;

        public NewBigNumberTest()
        {
            _bignum = new NewBigNumber();
        }

        [TestMethod]
        public void LongString_Sum_ShortString_Successed()
        {
            Assert.AreEqual("95473581679103661842681559839926449412282827939030990354079855099280761746",
                _bignum.Sum("95473581679103661842681559839926449412282827939030946487396800944958402058"
                , "43866683054154322359688"));
        }

        [TestMethod]
        public void LongString_SumChar_ShortString_Successed()
        {
            Assert.AreEqual("95473581679103661842681559839926449412282827939030990354079855099280761746",
                _bignum.SumChar("95473581679103661842681559839926449412282827939030946487396800944958402058"
                , "43866683054154322359688"));
        }

        [TestMethod]
        public void HasCarry_SumChar_Successed()
        {
            Assert.AreEqual("1979595795759579795795797957957975956",
                _bignum.SumChar("989797897879789897897898978978987978"
                , "989797897879789897897898978978987978"));
        }

        [TestMethod]
        public void HasCarry_Sum_Successed()
        {
            Assert.AreEqual("1979595795759579795795797957957975956",
                _bignum.Sum("989797897879789897897898978978987978"
                , "989797897879789897897898978978987978"));
        }

        [TestMethod]
        public void Five_Sum_Five_Successed()
        {
            Assert.AreEqual("11111111111111111111111111111111110",
                _bignum.Sum("5555555555555555555555555555555555"
                , "5555555555555555555555555555555555"));
        }

        [TestMethod]
        public void Five_SumChar_Five_Successed()
        {
            Assert.AreEqual("11111111111111111111111111111111110",
                _bignum.SumChar("5555555555555555555555555555555555"
                , "5555555555555555555555555555555555"));
        }

        [TestMethod]
        public void Sum_Zero_Successed()
        {
            Assert.AreEqual("78091310475042957389080284983864874191263692947",
                _bignum.Sum("78091310475042957389080284983864874191263692947"
                , "0"));
        }

        [TestMethod]
        public void SumChar_Zero_Successed()
        {
            Assert.AreEqual("78091310475042957389080284983864874191263692947",
                _bignum.SumChar("78091310475042957389080284983864874191263692947"
                , "0"));
        }

        [TestMethod]
        public void Zero_Sum_Zero_Successed()
        {
            Assert.AreEqual("0",
                _bignum.Sum("0"
                , "0"));
        }

        [TestMethod]
        public void Zero_SumChar_Zero_Successed()
        {
            Assert.AreEqual("0",
                _bignum.SumChar("0"
                , "0"));
        }

        [TestMethod]
        public void Five_Sum_Five_Failed()
        {
            Assert.AreNotEqual("11111111111111111111111111111111110",
                _bignum.Sum("5555555555555555555555555555555555"
                , "5555555555555555555555555555555555"));
        }
    }
}
