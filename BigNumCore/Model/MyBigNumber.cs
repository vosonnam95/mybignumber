﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace BigNumCore.Model
{
    public class MyBigNumber : BigNumber,IBigNum
    {

        public MyBigNumber()
        {
        }

        public double Num9
        {
            get
            {
                return (int)Math.Pow(10, 9);
            }
        }

        public string SumChar(string a, string b)
        {
            base.isVailedPositiveIntegers(a, b);
            int carry = 0;
            int lengthMax=Math.Max(a.Length, b.Length);
            a = a.PadLeft(lengthMax, '0');
            b = b.PadLeft(lengthMax, '0');
            var numbers = Enumerable.Range(0, lengthMax)
                .Reverse()
                .Select(i => new
                {
                    A = a[i] - '0',
                    B = b[i] - '0'
                });
            var result = numbers.Select(num =>
            {
                int sum = num.A + num.B + carry;
                carry = sum / 10;
                return sum % 10;
            }).Reverse().ToList();
            if(carry>0)
                result.Insert(0,carry);
            return string.Join("", result);
        }

        public string Sum(string a, string b)
        {
            base.isVailedPositiveIntegers(a, b);
            double carry = 0;
            int i;
            List<string> rs = new List<string>();
            string[] StringA = SplitStr(a).ToArray(), StringB = SplitStr(b).ToArray();

            for (i = 0; !(i >= StringA.Length && i >= StringB.Length && carry == 0); i++)
            {
                double A = ParseInt(StringA, i), B = ParseInt(StringB, i);
                long sum = ((long)(A + B + carry));
                carry = GetCarry(sum);
                rs.Insert(0, GetString(sum));
            }
            rs[0] = double.Parse(rs[0]).ToString();
            return String.Join("", rs);
        }

        public string Subtract(string a, string b)
        {
            base.isVailedPositiveIntegers(a, b);
            int i = 0,
                _isGreater = CompareNumber(a, b);
            if (_isGreater == 0)
                return "0";
            double _carry = 0;
            List<string> rs = new List<string>();
            string[] StringA = SplitStr((_isGreater > 0) ? a : b).ToArray(),
                StringB = SplitStr((_isGreater > 0) ? b : a).ToArray();

            for (i = 0; !(i >= StringA.Length && i >= StringB.Length && _carry == 0); i++)
            {
                double A = ParseInt(StringA, i), B = ParseInt(StringB, i);
                int _borrow = 0;
                if (A < B)
                    _borrow = 1;
                long sum = ((long)(A + (-B) + _carry + _borrow * Num9));
                _carry = GetCarry(sum) + _borrow * (-1);
                rs.Insert(0, GetString(sum));
            }
            string _result = String.Join("", rs);
            _result = RemoveZeroAtStart(_result);
            if (_isGreater < 0)
                return $"-{_result}";
            return _result;
        }

        private static string RemoveZeroAtStart(string str)
        {
            Regex ZeroStartRegex = new Regex(@"^[0]+");
            str = ZeroStartRegex.Replace(str, "");
            if (string.IsNullOrEmpty(str))
                return "0";
            return str;
        }

        public BigInteger AddNum(BigInteger integerA, BigInteger integerB)
        {
            string _result = string.Empty;
            if(integerA.Minus>=0 && integerB.Minus >= 0)
            {
                _result = Sum(integerA.Number, integerB.Number);
                return new BigInteger(1, _result);
            }
            if (integerA.Minus <= 0 && integerB.Minus <= 0)
            {
                _result = Sum(integerA.Number, integerB.Number);
                return new BigInteger(-1, _result);
            }
            if (integerA.Minus >= 0 && integerB.Minus <= 0)
            {
                _result = Subtract(integerA.Number, integerB.Number);
                return new BigInteger(_result);
            }
            if (integerA.Minus <= 0 && integerB.Minus >= 0)
            {
                _result = Subtract(integerB.Number, integerA.Number);
                return new BigInteger(_result);
            }

            return new BigInteger(1, "0");
        }
        /// <summary>
        /// compare two string number
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns>
        /// return 1 if a greater than b
        /// return -1 if a lesser than b
        /// return 0 if a equal b
        /// </returns>
        private int CompareNumber(string a, string b)
        {
            if (a.Length > b.Length)
                return 1;
            if (a.Length < b.Length)
                return -1;
            for (int i=0; i<a.Length; i++)
            {
                if ((a[i] - b[i]) > 0)
                    return 1;
                if ((a[i] - b[i]) < 0)
                    return -1;
            }
            return 0;
        }
        /// <summary>
        /// parse a long number to string with format
        /// </summary>
        /// <param name="sum"></param>
        /// <returns>return string with length equal base number</returns>
        private string GetString(long sum)
        {
            if (sum < 0)
                return sum.ToString("D10").Substring(2, 9);
            return sum.ToString("D10").Substring(1, 9);
        }
        /// <summary>
        /// get a item and parse to double from array string
        /// </summary>
        /// <param name="str"></param>
        /// <param name="index"></param>
        /// <returns> return a double</returns>
        private double ParseInt(string[] str, int index)
        {
            if (index >= str.Length)
                return 0;
            return double.Parse(str[index]);
        }
        /// <summary>
        /// get carry number from double number with base number is setted
        /// </summary>
        /// <param name="num"></param>
        /// <returns>
        /// return double number
        /// </returns>
        private double GetCarry(long num)
        {
            if (num < 0)
                return Math.Ceiling(num / Num9);
            return Math.Floor(num / Num9);
        }

        /// <summary>
        /// Split a string into chunks of a certain size
        /// </summary>
        /// <param name="str"></param>
        /// <param name="size"></param>
        /// <returns> return list of reverse string is splitted into chunks  </returns>
        private List<string> SplitStr(string str, int size = 9)
        {
            List<string> rs = new List<string>();
            int i = str.Length;
            do
            {
                if (str.Length < 9)
                    break;
                i = i - 9;
                rs.Add(str.Substring(i, 9));
            } while (i >= 9);
            if (str.Length % 9 > 0)
                rs.Add(str.Substring(0, str.Length % 9));
            return rs;
        }
    }
}
