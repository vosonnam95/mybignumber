﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BigNumCore.Model
{
    public class BigNumber
    {
        public virtual void isVailedPositiveIntegers(string a, string b)
        {
            if ((string.IsNullOrEmpty(a) || string.IsNullOrEmpty(b)))
                throw new ArgumentNullException("Number must be not null!");
            var isPositiveInteger = new Regex(@"^([1-9]\d+)|0$");
            if ((!isPositiveInteger.IsMatch(a.Trim())|| !isPositiveInteger.IsMatch(b.Trim())))
                throw new FormatException("Number must be Positive Integer!");
        }
    }
}
