﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigNumCore.Model
{
    public interface IBigNum
    {
        string SumChar(string a, string b);
        BigInteger AddNum(BigInteger integerA, BigInteger integerB);
        string Sum(string a, string b);
        string Subtract(string a, string b);
    }
}
