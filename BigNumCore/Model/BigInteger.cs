﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BigNumCore.Model
{
    public class BigInteger
    {
        private static string pattern = @"^(?<minus>-)?(?<number>[1-9]\d*)$|(?<number>^0$)";
        private string _number;
        private int _minus;

        public BigInteger(string num)
        {
            var _match = GetRegexMatch(num);
            if (!_match.Success)
                throw new FormatException("Input string was not in a correct format.");
            this.Minus = _match.Groups["minus"].Success ? -1 : 1;
            this.Number = _match.Groups["number"].Value;
        }

        public BigInteger(int minus,string number)
        {
            if (minus < 0 && "0".Equals(number))
                throw new ArgumentException("Zero was not Negative Number");
            var _match = GetRegexMatch(number);
            if (!_match.Success || _match.Groups["minus"].Success)
                throw new FormatException("Input string was not in a correct format.");
            this.Minus = minus;
            this.Number = _match.Groups["number"].Value;
        }

        public string Number {
            get
            {
                return _number;
            }
            private set
            {
                _number = value;
            }
        }

        public bool IsNegative {
            get
            {
                return _minus < 0;
            }
        }

        public int Minus
        {
            get
            {
                return _minus;
            }
            private set
            {
                _minus = value;
            }
        }
        public static bool IsInteger(string str)
        {
            Regex _regex = new Regex(pattern);
            return _regex.IsMatch(str);
        }

        private Match GetRegexMatch(string str)
        {
            Regex IntegerRegex = new Regex(pattern);
            return IntegerRegex.Match(str);
        }

        public override string ToString()
        {
            if (IsNegative)
                return $"-{Number}";
            return Number;
        }

    }
}
