﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BigNumCore.Model
{
    public class TestBigNumber: BigNumber, IBigNum
    {

        public TestBigNumber()
        {
        }

        public double Num9
        {
            get
            {
                return (int)Math.Pow(10, 9);
            }
        }

        public string SumChar(string a, string b)
        {
            throw new NotImplementedException("Operate are not Implemented");
        }

        public string Sum(string a, string b)
        {
            base.isVailedPositiveIntegers(a, b);
            double carry = 0;
            List<string> rs = new List<string>(),
                StringA = SplitStr(a), 
                StringB = SplitStr(b);
            int aLength=StringA.Count(),
                bLength=StringB.Count(),
                MaxLength = 0;
            MaxLength = Math.Max(aLength, bLength);
            rs = Enumerable.Range(0, MaxLength).Select(_index =>
            {
                if (_index >= aLength && carry == 0)
                    return StringB[_index];
                if (_index >= bLength && carry == 0)
                    return StringA[_index];
                double A = ParseInt(StringA,_index),
                    B = ParseInt(StringB, _index);
                long sum = (long)(A + B + carry);
                carry = GetCarry(sum);
                return GetString(sum);
            }).Reverse().ToList();
            if (carry > 0)
            {
                rs.Insert(0, "" + carry);
                carry = 0;
            }
            string _result = String.Join("", rs);
            _result = RemoveZeroAtStart(_result);
            return _result;
        }

        public string Subtract(string a, string b)
        {
            throw new NotImplementedException("Operate are not Implemented");
        }

        private static string RemoveZeroAtStart(string str)
        {
            Regex ZeroStartRegex = new Regex(@"^[0]+");
            str = ZeroStartRegex.Replace(str, "");
            if (string.IsNullOrEmpty(str))
                return "0";
            return str;
        }

        public BigInteger AddNum(BigInteger integerA, BigInteger integerB)
        {
            int _minus = 1,
                _aMinus = integerA.Minus,
                _bMinus = integerB.Minus;
            if (integerA.Minus * integerB.Minus < 0)
            {
                int _isGreater = CompareNumber(integerA.Number, integerB.Number);
                if (_isGreater == 0)
                    return new BigInteger(1, "0");
                _minus = _minus * _isGreater * integerA.Minus;
            }
            else
            {
                _minus = _minus * integerA.Minus;
            }

            string a = integerA.Number, b = integerB.Number;
            double carry = 0;
            List<string> StringA = SplitStr(a),
                StringB = SplitStr(b);
            int aLength = StringA.Count(),
                bLength = StringB.Count(),
                MaxLength = 0;
            MaxLength = Math.Max(aLength, bLength);
            var rs = Enumerable.Range(0, MaxLength).Select(_index =>
            {
                if (_index >= aLength && carry == 0)
                    return StringB[_index];
                if (_index >= bLength && carry == 0)
                    return StringA[_index];

                double A = ParseInt(StringA,_index),
                    B = ParseInt(StringB, _index), 
                    _sumAB = default;
                int _borrow = 0;

                _sumAB = (_aMinus * A + _bMinus * B) * _minus;
                if (_aMinus * _bMinus < 0 && _sumAB < 0)
                {
                    _borrow = 1;
                    _sumAB += _borrow * Num9;
                }

                long sum = (long)(_sumAB + carry);

                carry = GetCarry(sum) + _borrow * (-1);
                return GetString(sum);
            }).Reverse();
            if (carry > 0)
            {
                rs.Prepend("" + carry);
                carry = 0;
            }
            string _result = String.Join("", rs.ToList());
            _result = RemoveZeroAtStart(_result);

            return new BigInteger(_minus, _result);
        }
        /// <summary>
        /// compare two string number
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns>
        /// return 1 if a greater than b
        /// return -1 if a lesser than b
        /// return 0 if a equal b
        /// </returns>
        private int CompareNumber(string a, string b)
        {
            if (a.Length > b.Length)
                return 1;
            if (a.Length < b.Length)
                return -1;
            for (int i = 0; i < a.Length; i++)
            {
                if ((a[i] - b[i]) > 0)
                    return 1;
                if ((a[i] - b[i]) < 0)
                    return -1;
            }
            return 0;
        }
        /// <summary>
        /// parse a long number to string with format
        /// </summary>
        /// <param name="sum"></param>
        /// <returns>return string with length equal base number</returns>
        private string GetString(long sum)
        {
            if (sum < 0)
                return sum.ToString("D10").Substring(2, 9);
            return sum.ToString("D10").Substring(1, 9);
        }
        /// <summary>
        /// get a item and parse to double from array string
        /// </summary>
        /// <param name="str"></param>
        /// <param name="index"></param>
        /// <returns> return a double</returns>
        private double ParseInt(List<string> str, int index)
        {
            if (index >= str.Count())
                return 0;
            return double.Parse(str[index]);
        }
        /// <summary>
        /// get carry number from double number with base number is setted
        /// </summary>
        /// <param name="num"></param>
        /// <returns>
        /// return double number
        /// </returns>
        private double GetCarry(long num)
        {
            if (num < 0)
                return Math.Ceiling(num / Num9);
            return Math.Floor(num / Num9);
        }

        /// <summary>
        /// Split a string into chunks of a certain size
        /// </summary>
        /// <param name="str"></param>
        /// <param name="size"></param>
        /// <returns> return list of reverse string is splitted into chunks  </returns>
        private List<string> SplitStr(string str, int size = 9)
        {
            List<string> rs = new List<string>();
            int i = str.Length;
            do
            {
                if (str.Length < 9)
                    break;
                i = i - 9;
                rs.Add(str.Substring(i, 9));
            } while (i >= 9);
            if (str.Length % 9 > 0)
                rs.Add(str.Substring(0, str.Length % 9));
            return rs;
        }
    }
}
