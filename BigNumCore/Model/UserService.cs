﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Globalization;

namespace BigNumCore.Model
{
    public class UserService : IUser
    {
        private readonly IBigNum BigNum;

        public UserService(IBigNum _bigNum)
        {
            BigNum = _bigNum;
        }

        public string Desciptions()
        {
            List<string> _desciption = new List<string>() 
            {
                "perform operations between 2 or more integers",
                "\tMATH <LOG|FILE> <version> <operation> <arguments>",
                "\tLOG\tPrint result string into console shell",
                "\t\targuments: <number1> <number2> ... <numberN>",
                "\t\t\tnumberN: string integer",
                "\tFILE\tWrite result into file output",
                "\t\targuments: <inputfile> <outputfile> [--verbose]",
                "\t\t\tinputfile, outputfile:string path file,",
                "\t\t\t--verbose:print result writed into file to shell",
                "\tversion\tversion operations v1|v2|v3",
                "\toperation\toperation excuted SUM|SUMCHAR|SUBTRACT|ADDNUM"
            };
            return string.Join("\n", _desciption);
        }

        public void ExcuteFile(string method, string a, string b, bool isPrint=false)
        {
            var _results = new List<string>();
            try
            {
                FileInfo _inputFile = new FileInfo(a),
                    _outputFile = new FileInfo(b);
                var isExtension = new Regex(@"csv|txt",RegexOptions.IgnoreCase);
                if (!(isExtension.IsMatch(_inputFile.Extension) && isExtension.IsMatch(_outputFile.Extension)))
                    throw new FormatException("File must be text file!");
                if (!_inputFile.Exists)
                    throw new ArgumentNullException("Path File Input must not be null!");
                using (StreamReader streamReader = new StreamReader(_inputFile.OpenRead())) 
                {
                    string _line = string.Empty;
                    int _totalLine = 0, _totalNum = 0;
                    while ((_line = streamReader.ReadLine()) != null)
                    {
                        string[] numbers = _line.Split(',', ';','\t');
                        string _result = "0";
                        foreach(string _num in numbers)
                        {
                            string _sum = _result;
                            _result = Run(method, _sum, _num);
                        }
                        _totalLine++;
                        _totalNum += numbers.Length;
                        _results.Add(_result);
                    }
                    Console.WriteLine($"Total {_totalLine} lines, {_totalNum} Numbers");
                    streamReader.Close();
                };
                using(var streamWriter=new StreamWriter(_outputFile.OpenWrite())) 
                {
                    if (_results.Count() > 0)
                    {
                        streamWriter.Flush();
                        streamWriter.Write(string.Join("\n", _results));
                        Console.WriteLine($"Total write to file {_results.Count()} line Numbers");
                    }
                    streamWriter.Close();
                };

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                if(isPrint)
                    Console.WriteLine(string.Join("\n", _results));
            }
        }

        public string ExcuteString(string method, string[] numbers)
        {
            string _result = "0";
            foreach (string _num in numbers)
            {
                string mem = _result;
                _result = Run(method, mem, _num);
            }
            return string.Join("\n", _result);
        }

        private string Run(string method, string a, string b)
        {
            method = method.ToUpper();
            if ("SUM".Equals(method))
            {
                return BigNum.Sum(a, b);
            }
            if ("SUMCHAR".Equals(method))
            {
                return BigNum.SumChar(a, b);
            }
            if ("ADDNUM".Equals(method))
            {
                return BigNum.AddNum(new BigInteger(a), new BigInteger(b)).ToString();
            }
            if ("SUBTRACT".Equals(method))
            {
                a = a.StartsWith('-') ? a.Remove(0,1) : a;
                return BigNum.Subtract(a, b);
            }
            return "";
        }
    }
}
