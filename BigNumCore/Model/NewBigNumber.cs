﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigNumCore.Model
{
    public class NewBigNumber : BigNumber, IBigNum
    {

        public BigInteger AddNum(BigInteger integerA, BigInteger integerB)
        {
            throw new NotImplementedException("Operate are not Implemented");
        }

        public string Subtract(string a, string b)
        {
            throw new NotImplementedException("Operate are not Implemented");
        }

        public string Sum(string a, string b)
        {
            base.isVailedPositiveIntegers(a, b);
            int carry = 0;
            int maxLength = Math.Max(a.Length, b.Length);
            a = a.PadLeft(maxLength, '0');
            b = b.PadLeft(maxLength, '0');
            var numbers = Enumerable.Range(0, maxLength)
                .Reverse()
                .Select(i => new
                {
                    A = a[i] - '0',
                    B = b[i] - '0'
                });
            var result = numbers.Select(num =>
            {
                int sum = num.A + num.B + carry;
                carry = sum / 10;
                return sum % 10;
            }).Reverse().ToList();
            if (carry > 0)
                result.Insert(0, carry);
            return string.Join("", result);
        }

        public string SumChar(string a, string b)
        {
            base.isVailedPositiveIntegers(a, b);
            int number1Len = a.Length, number2Len = b.Length;
            int lenMax = number1Len > number2Len ? number1Len : number2Len;
            string result = String.Empty;
            int temp;
            int idx1, idx2;
            char c1, c2;
            int d1, d2;
            int mem = 0;
            for (int i = 0; i < lenMax; i++)
            {
                idx1 = number1Len - i - 1;
                idx2 = number2Len - i - 1;
                c1 = (idx1 >= 0) ? a[idx1] : '0';
                c2 = (idx2 >= 0) ? b[idx2] : '0';
                d1 = c1 - '0';
                d2 = c2 - '0';
                temp = d1 + d2 + mem;
                mem = temp / 10;
                temp = temp % 10;
                result = temp + result;
            }
            if (mem > 0) { result = mem + result; }
            return result;
        }
    }
}
