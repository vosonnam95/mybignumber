﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigNumCore.Model
{
    public interface IUser
    {
        string ExcuteString(string method, string[] numbers);
        void ExcuteFile(string method, string a, string b, bool isPrint);
        string Desciptions();
    }
}
