﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigNumCore.Model
{
    public class Number
    {
        public Number()
        {
        }

        public Number(string numberA, string numberB)
        {
            NumberA = numberA;
            NumberB = numberB;
        }

        public string NumberA { get; set; }
        public string NumberB { get; set; }
    }
}
