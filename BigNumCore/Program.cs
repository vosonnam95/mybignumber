﻿using BigNumCore.Model;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace BigNumCore
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {

                UserService _service = new UserService(new MyBigNumber());
                string _helper = (args.Length == 1) ? args[0] : "";
                if ("/?".Equals(_helper))
                {
                    Console.WriteLine(_service.Desciptions());
                    return;
                }
                if (args.Length < 5)
                    throw new ArgumentNullException("Arguments too short!");

                string _type = args[0].ToUpper(),
                    _version = args[1].ToUpper(),
                    _operate = args[2].ToUpper();
                string[] _arguments = args.Skip(3).ToArray();

                if ((string.IsNullOrEmpty(_type) && string.IsNullOrEmpty(_version) && string.IsNullOrEmpty(_operate)))
                    throw new ArgumentNullException("Arguments is required!");
                bool isNotVailedMethod = !_operate.Equals("SUM") &&
                    !_operate.Equals("SUMCHAR") &&
                    !_operate.Equals("SUBTRACT") &&
                    !_operate.Equals("ADDNUM");
                if (isNotVailedMethod)
                    throw new FormatException(" Operation must be SUM|SUMCHAR|SUBTRACT|ADDNUM");
                if ("V1".Equals(_version))
                    _service = new UserService(new MyBigNumber());
                else if ("V2".Equals(_version))
                    _service = new UserService(new NewBigNumber());
                else if ("V3".Equals(_version))
                    _service = new UserService(new TestBigNumber());
                if ("FILE".Equals(_type))
                {
                    string _input = _arguments[0],
                        _output = _arguments[1],
                        _verbose = (_arguments.Length == 3) ? _arguments[2].ToUpper() : "";
                    bool isPrint = "--VERBOSE".Equals(_verbose);
                    _service.ExcuteFile(_operate, _input, _output, isPrint);
                }
                else if ("LOG".Equals(_type))
                {
                    //Console.WriteLine(string.Join(",", _arguments));
                    if (_arguments.Length > 1)
                        Console.WriteLine(_service.ExcuteString(_operate, _arguments));
                }
            }catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            //using var loggerFactory = LoggerFactory.Create(builder =>
            //{
            //    builder
            //        .AddFilter("Microsoft", LogLevel.Warning)
            //        .AddFilter("System", LogLevel.Warning)
            //        .AddFilter("NonHostConsoleApp.Program", LogLevel.Debug)
            //        .AddConsole();
            //});
            //ILogger logger = loggerFactory.CreateLogger<Program>();
            //logger.LogInformation("[Log]:"+b.AddNum(new BigInteger("-123400000000000"),new BigInteger("78900000000000")));
            //logger.LogInformation("[Log]:" + b.Sum("21621122121321321321321323213213200351650213211021045375676725778",
            //    "12221212122151005456105465422056465477878227978452345312"));
            //var strChunk = "12221212122151005456105465422056465477878227978452345312".Chunk(9).Select(i=>string.Join("",i));
            //logger.LogInformation("[Log] strChunk:" +string.Join("\n",strChunk));
        }
    }
}
